package com.songoda.epicspawners.hook;

public enum HookType {

    FACTION, TOWN, ISLAND, REGULAR
}
